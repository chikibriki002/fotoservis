package com.chikieblan4ik.photoservice;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class AdapterPhotos extends RecyclerView.Adapter<AdapterPhotos.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<Photography> photos;

    public AdapterPhotos(Context context, ArrayList<Photography> photos) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.photos = photos;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= inflater.inflate(R.layout.item_photo, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final Photography onePhoto= photos.get(position);
        Glide.with(context).load(onePhoto.getUrl()).into(holder.photo);
        //rx java retrofit
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView photo;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
//            tvTypeCard = itemView.findViewById(R.id.tvTypeCard);
            photo = itemView.findViewById(R.id.photo);
        }
    }
}