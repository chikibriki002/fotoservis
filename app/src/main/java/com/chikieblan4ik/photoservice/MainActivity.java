package com.chikieblan4ik.photoservice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "main";

    EditText etLogin;
    EditText etPassword;
    Button btnSignin;
    TextView btnGoToReg;

    String url = "https://reqres.in/api/login";

    User user = new User();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        etLogin = findViewById(R.id.etLogin);
        etPassword = findViewById(R.id.etPassword);
        btnSignin = findViewById(R.id.btnSignIn);
        btnGoToReg = findViewById(R.id.btnGoToReg);

        btnGoToReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(MainActivity.this, Registration.class);
                Intent intent = new Intent(MainActivity.this, Registration.class);
                startActivity(intent);
            }
        });

        btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSignIn();
            }
        });
    }

    public void onClickSignIn() {
        String login = etLogin.getText().toString();
        String password = etPassword.getText().toString();

        Map<String, String> keysValues = new HashMap<>();
//        "email": "eve.holt@reqres.in",
//        "password": "cityslicka"
        keysValues.put("email", "eve.holt@reqres.in");
        keysValues.put("password", "cityslicka");
//        keysValues.put("login", login);
//        keysValues.put("password", password);

        String responseApi = Api.request(keysValues, "POST", "login");

        Log.i(TAG, "onClickSignIn: " + responseApi);
        try {
            JSONObject response = new JSONObject(responseApi);
            String token = response.getString("token");
            Log.i(TAG, "onClickSignIn: " + token);
            // тут должен быть обработчик ошибок, а не это
            // тут должен быть обработчик ошибок, а не это
            // тут должен быть обработчик ошибок, а не это
            if (token != null) {
                user.setToken(token);
                user.getUserPhotos();

                Intent intent = new Intent(MainActivity.this, Gallery.class);
                intent.putExtra("user", user);
                startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "onClickSignIn: " + e);
        }
    }

}