package com.chikieblan4ik.photoservice;

import android.util.Log;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Link {

    private static final String TAG = "link";
    String url;
    Map<String, String> keysValues;

    public Link(Map<String, String> keysValues, String url) {
        this.url = url;
        if (keysValues != null)
            this.keysValues = keysValues;
    }


}
