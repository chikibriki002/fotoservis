package com.chikieblan4ik.photoservice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class DownloadPhoto extends AppCompatActivity implements View.OnClickListener {

    ImageView btnAddPhoto;
    TextView tvGoToGallery;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_photo);
        getSupportActionBar().hide();
        btnAddPhoto = findViewById(R.id.imgAddPhoto);
        tvGoToGallery = findViewById(R.id.tvGoToGalery);

        user = getIntent().getParcelableExtra("user");

        btnAddPhoto.setOnClickListener(this);

        tvGoToGallery.setOnClickListener(this);

        // о_0
    }

    public void downloadPhoto() {
        // открыть галерею, выбранное фото превратить в файл, создать объект Photography
        // вызвать у него метод downloadPhoto()
    }

    @Override
    public void onClick(View v) {
        // из галереи берем фото
        int id = user.getPhotographies().size() + 1;
        Photography photo = new Photography(id, "https://placegoat.com/200/200"); // тут будет ссылкана фото, которая вернется из апи
        String url = photo.downloadPhotoOnServer();
        user.addOnePhoto(photo);

        Intent intent = new Intent();
        intent.putExtra("user", user);
        setResult(RESULT_OK, intent);
        finish();
    }
}