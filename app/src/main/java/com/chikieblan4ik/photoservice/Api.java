package com.chikieblan4ik.photoservice;

import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class Api {

    private static final String TAG = "api123";

    private static String url = "https://reqres.in/api/"; // начало ссылки

    public static String responseFromStream(BufferedReader in) {
        try {
            StringBuilder builder = new StringBuilder();
            String temp;

            while ((temp = in.readLine()) != null) {
                builder.append(temp);
            }
            Log.i(TAG, "zesponseFromStream: " + builder);
            return builder.toString();
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "responseFromStream: " + e);
            return null;
        }
    }

    public static String getUrlParams(Map<String, String> keysValues, String method) {
        try {
            switch (method) {
                case "PUT":
                case "POST":
                    return new JSONObject(keysValues).toString();
                case "GET":
                    StringBuilder constructor = new StringBuilder();
                    Iterator iterator = keysValues.keySet().iterator();

                    while (iterator.hasNext()) {
                        String key = iterator.next().toString();
                        constructor.append(key);
                        constructor.append("=");
                        constructor.append(keysValues.get(key));
                        constructor.append("&");
                    }

                    return constructor.substring(0, constructor.length() - 1);
                default:
                    return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "getUrlParams: " + e);
            return null;
        }
    }

    static String response;
    public static String request(final Map<String, String> keysValues, final String capsTypeRequest, final String methodApi) {
        response = null;
        Runnable run = new Runnable() {
            @Override
            public void run() {
                try {
                    String myUrl = url + methodApi;
                    if (capsTypeRequest.equals("DELETE")) {
                        myUrl = myUrl + "/" + keysValues.get("id");
                    }
                    if (capsTypeRequest.equals("GET")) {
                        myUrl = myUrl + "?" + getUrlParams(keysValues, capsTypeRequest);
                    }

                    HttpURLConnection connection = (HttpURLConnection) new URL(myUrl).openConnection();
                    if (!capsTypeRequest.equals("GET")) {
                        connection.setRequestProperty("Content-type", "application/json; charset=utf-8");
                        connection.setRequestMethod(capsTypeRequest);
                        connection.setDoOutput(true);
                    }
                    if (!capsTypeRequest.equals("DELETE") & !capsTypeRequest.equals("GET")) {
                        connection.getOutputStream().write(getUrlParams(keysValues, capsTypeRequest).getBytes());
                    }
                    Log.i(TAG, "code: " + connection.getResponseCode());

                    BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    //response = new JSONObject(responseFromStream(in));
                    //response = responseFromStream(in);
                    String temp = responseFromStream(in);
                    response = temp.substring(0, temp.length() - 1) + ",\"code\":" + connection.getResponseCode() + "}";
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i(TAG, "runRequest: " + e);
                }
            }
        };

        Thread thread = new Thread(run);
        try {
            thread.start();
            thread.join();
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "requestThread: " + e);
        }
        return response;
    }
}


//        if (capsTypeRequest.equals("GET")) {
//            String myUrl = url + methodApi + getUrlParams(keysValues, capsTypeRequest);
//            HttpURLConnection connection = (HttpURLConnection) new URL(myUrl).openConnection();
//            BufferedReader in1 = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//            response = responseFromStream(in1);
//        }




















//    static JSONObject response;
//    public static JSONObject request(final Map<String, String> keysValues, final String capsMethod, final String methodUrlApi) {
//        response = null;
//        Runnable run = new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    String myUrl = url + methodUrlApi;
//                    if (capsMethod.equals("DELETE")) {
//                        myUrl = myUrl + "/" + keysValues.get("id");
//                    }
//
//                    HttpURLConnection connection = (HttpURLConnection) new URL(myUrl).openConnection();
//
//                    connection.setRequestProperty("Content-type", "application/json; charset=utf-8");
//                    connection.setRequestMethod(capsMethod);
//                    connection.setDoOutput(true);
//                    if (!capsMethod.equals("DELETE") & keysValues.size() > 0) {
//                        connection.getOutputStream().write(getUrlParams(keysValues, capsMethod).getBytes());
//                    }
//
//                    BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//                    try {
//                        response = new JSONObject(responseFromStream(in));
//                        response.put("Code", connection.getResponseCode());
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                        Log.i(TAG, "run: " + e);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    Log.i(TAG, "run post: " + e);
//                }
//            }
//        };
//
//        Thread threadPost = new Thread(run);
//        try {
//            threadPost.start();
//            threadPost.join();
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.i(TAG, "post thread: " + e);
//        }
//        return response;
//    }
//}
