package com.chikieblan4ik.photoservice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Registration extends AppCompatActivity {

    public static final String TAG = "reg";
    EditText etName;
    EditText etSurname;
    EditText etPhone;
    EditText etPassword;
    EditText etPassword2;
    Button btnReg;
    TextView tvGoToLogin;

    String url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        etName = findViewById(R.id.etName);
        etSurname = findViewById(R.id.etSurname);
        etPhone = findViewById(R.id.etPhone);
        etPassword = findViewById(R.id.etPasswordR);
        etPassword2 = findViewById(R.id.etPassword2R);
        btnReg = findViewById(R.id.btnReg);
        tvGoToLogin = findViewById(R.id.tvGoToSignIn);

        tvGoToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Registration.this, MainActivity.class);
                startActivity(intent);
            }
        });

        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickRegistr();
            }
        });
    }

    public void onClickRegistr() {
        Map<String, String> keysValues = new HashMap<>();
        keysValues.put("first_name", etName.getText().toString());
        keysValues.put("surname", etSurname.getText().toString());
        keysValues.put("phone", etPhone.getText().toString());
        keysValues.put("password", etPassword.getText().toString());

        String responseApi = Api.request(keysValues, "POST", "signup");
        try {
            JSONObject response = new JSONObject(responseApi);
            int id = response.getInt("id");
            // тут должен быть обработчик ошибок, а не это
            // тут должен быть обработчик ошибок, а не это
            // тут должен быть обработчик ошибок, а не это
            if (id != 0) {
                // не очень понятно
                User user = new User(keysValues);
                Intent intent = new Intent(Registration.this, MainActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(this, "блин ну короче регистрация не оч", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "onClickSignIn: " + e);
        }
    }
}