package com.chikieblan4ik.photoservice;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class User implements Parcelable {

    public static final String TAG = "user";
    private String name;
    private String surname;
    private String phone;
    private String token;
    private int id;
    private ArrayList<Photography> photographies;

    public User() {}

    public User(Map<String, String> keysValues) {
        photographies = new ArrayList<>();
        this.name = keysValues.get("first_name");
        this.surname = keysValues.get("surname");
        this.phone = keysValues.get("phone");
    }

    protected User(Parcel in) {
        name = in.readString();
        surname = in.readString();
        phone = in.readString();
        token = in.readString();
        id = in.readInt();
        photographies = in.createTypedArrayList(Photography.CREATOR);
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public void getUserPhotos() {
//        if (photographies == null) {
//            String url = "";
//            Map<String, String> map = new HashMap<>();
//            Link link = new Link(map, url);
//            String responseUrl = new Api().get(link);
//            try {
//                JSONObject response = new JSONObject(responseUrl);
//                JSONArray array = response.getJSONArray(""); // в зависимости от того, что придет в json ответе
//                for (int i = 0; i < array.length(); i++) {
//                    Photography photo = new Photography(); // в зависимости от того, что придет в json ответе
//                    photographies.add(photo);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//                Log.i(TAG, "getUserPhotos: " + e);
//            }
//        }
        ArrayList<Photography> photographyArrrayList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Photography photo = new Photography(i, "https://placegoat.com/200/200");
            photographyArrrayList.add(photo);
        }

        setPhotographies(photographyArrrayList);
    }

    public void addOnePhoto(Photography photography) {
        photographies.add(photography);
    }

    public void deletePhoto(int id) {
        photographies.get(id).deletePhoto();
        // что-то вроде этого
    }




    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPhone() {
        return phone;
    }

    public String getToken() {
        return token;
    }

    public int getId() {
        return id;
    }

    public ArrayList<Photography> getPhotographies() {
        return photographies;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPhotographies(ArrayList<Photography> photographies) {
        this.photographies = photographies;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(surname);
        dest.writeString(phone);
        dest.writeString(token);
        dest.writeInt(id);
        dest.writeTypedList(photographies);
    }
}
