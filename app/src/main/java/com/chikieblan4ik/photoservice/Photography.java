package com.chikieblan4ik.photoservice;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Photography implements Parcelable {

    private static final String TAG = "photo";
    private int id;
    private int ownerID;
    private String name;
    private String url;
    private ArrayList<User> usersWhoCanWatch;

    public Photography(int id, String url) {
        this.id = id;
        this.url = url;
    }

    protected Photography(Parcel in) {
        id = in.readInt();
        ownerID = in.readInt();
        name = in.readString();
        url = in.readString();
        usersWhoCanWatch = in.createTypedArrayList(User.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(ownerID);
        dest.writeString(name);
        dest.writeString(url);
        dest.writeTypedList(usersWhoCanWatch);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Photography> CREATOR = new Creator<Photography>() {
        @Override
        public Photography createFromParcel(Parcel in) {
            return new Photography(in);
        }

        @Override
        public Photography[] newArray(int size) {
            return new Photography[size];
        }
    };

    public void deletePhoto() {

    }

    public String downloadPhotoOnServer() { // вернет ссылку на фотографию
        return null;
    }

    public int getId() {
        return id;
    }

    public int getOwnerID() {
        return ownerID;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public ArrayList<User> getUsersCanWatch() {
        return usersWhoCanWatch;
    }


}
