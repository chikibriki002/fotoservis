package com.chikieblan4ik.photoservice;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.chikieblan4ik.photoservice.R;

public class Gallery extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "gallery";
    TextView btnDownloadPhoto;
    RecyclerView rvPhotos;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_gallery);
        rvPhotos = findViewById(R.id.rvPhotos);
        btnDownloadPhoto = findViewById(R.id.tvDownloadPhoto);

        user = getIntent().getParcelableExtra("user");

        btnDownloadPhoto.setOnClickListener(this);

        rvPhotos.setLayoutManager(new LinearLayoutManager(this));
        AdapterPhotos adapterPhotos = new AdapterPhotos(this, user.getPhotographies());
        rvPhotos.setAdapter(adapterPhotos);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(Gallery.this, DownloadPhoto.class);
        intent.putExtra("user", user);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) {
            return;
        }

        user = data.getParcelableExtra("user");
        AdapterPhotos adapterPhotos = new AdapterPhotos(this, user.getPhotographies());
        rvPhotos.setAdapter(adapterPhotos);
    }
}